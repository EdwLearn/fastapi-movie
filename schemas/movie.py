from pydantic import BaseModel, Field

# Importaciones de typing
from typing import Optional

class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=3, max_length=15)
    overview: str = Field(min_length=15, max_length=50)
    year: int = Field(le=2022)
    rating: float = Field(ge= 1,le=10)
    category: str = Field(min_length=3, max_length=10)
    
    class Config:
        extra = "allow"
        json_schema_extra = {
            "example": {
                "id": 1,
                "title": "Mi Pelicula",
                "overview": "Descripción de La pelicula",
                "year": 2022,
                "rating": 7.8,
                "category": "Acción"
            }
        }
