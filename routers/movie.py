from fastapi import APIRouter
# Importaciones de FastAPI
from fastapi import Depends, Path, Query, HTTPException
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder


# Importaciones de typing
from typing import List

# Importaciones propias
from config.database import Session
from models.movie import Movie as MovieModel
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()

DB = Session()
ID_EXCEPTION = HTTPException(status_code=404, detail= 'Invalid movie ID: ()')



@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code= 200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    result = MovieService(DB).get_movies()
    return JSONResponse(status_code=200,content = jsonable_encoder(result))

@movie_router.get('/movies/{id}', tags =['movies'], response_model=Movie)
def get_movies_by_id(id: int = Path(ge=1, le=2000)) -> Movie:
    result = MovieService(DB).get_movie_id(id)
    if not result:
        return JSONResponse(status_code=404, content={'Mesassage': 'Not Found'})
    return JSONResponse(status_code=200,content =jsonable_encoder(result))
    
@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie])
def get_movies_by_category(category: str = Query(min_length= 5, max_length=10)) -> List[Movie]:
    result = MovieService(DB).get_movie_category(category)
    if not result:
        return JSONResponse(status_code=404, content={'Mesassage': 'Not Found'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.post('/movies', tags=['movies'], response_model=dict, status_code=201)
def create_movie(movie: Movie) -> dict:
    MovieService(DB).create_movie(movie)
    return JSONResponse(status_code=201,content={"message": "Se registró la pelicula"})

@movie_router.put("/movies/{id}",tags=['movies'], response_model=dict, status_code=200)
def update_movie(id:int, movie: Movie) -> dict:
    result = MovieService(DB).get_movie_id(id)
    if not result:
        raise ID_EXCEPTION
    MovieService(DB).update_movie(id,movie)
    return JSONResponse(status_code=200,content={"message": "Se ha madificado la pelicula"})

@movie_router.delete('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def delete_movie(id: int) -> dict:
    result: MovieModel = DB.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        raise ID_EXCEPTION
    MovieService(DB).delete_movie(id)
    return JSONResponse(status_code=200,content={"message": "Se ha eliminado la pelicula"})
